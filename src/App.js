import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// import './App.css';
import LandingPages from "./pages/HomePage";
import PageDashboard from "./pages/DashboardPage/Dash";
import { DashHome } from "./pages/DashboardPage/DashAdmin";
import ListPage from "./pages/DashboardPage/ListCar";
import Detail from './pages/DetailPage'
import AddPage from "./pages/AddPage";
import InvoicePage from "./pages/InvoicePage";


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPages />} ></Route>
        <Route element={<PageDashboard/>} >
            <Route path="dashboard" element={<DashHome/>}></Route>
            <Route path="dashboard/listCar" element={<ListPage/>}></Route>
        </Route>
            <Route path="dashboard/listCar/add" element={<AddPage/>}></Route>
            <Route path="/invoice" element={<InvoicePage />}></Route>
            <Route path="/detail/id" element={<Detail />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
