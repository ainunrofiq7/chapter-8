import React, {Fragment} from "react"
import { Worker, Viewer } from '@react-pdf-viewer/core'
import '@react-pdf-viewer/core/lib/styles/index.css'
import { getFilePlugin } from "@react-pdf-viewer/get-file"
import file from '../../asset/Invoice.pdf'

import './Tiket.css'

  
const Tiket = () => {
    const getFilePluginInstance = getFilePlugin();
    const { Download } = getFilePluginInstance;

    return (
        <>
        <Fragment>

            <div className='header-tiket flex flex-col justify-between h-screen '>
                <div className='d-flex container  px-56 mb-3 justify-content-between align-items-start' >
                    <table>
                        <tr className='flex flex-row gap-4 items-center'>
                            <td><img src='/image/fi_arrow-left.svg' alt='Arrow Left' className='me-2'/>&nbsp;</td>
                            <span className='font-bold text-lg'>Tiket</span>
                        </tr>
                        <tr>
                            <td></td>
                            <p className="id">Order ID: xxxxxxx</p>
                        </tr>
                    </table>
                            <img src='/image/Frame 19.svg' alt='Steps'/>
                </div>
            </div>
            <div className='content-car d-flex justify-content-center align-items-center'>
                <div className='w-75'>
                    <div className='row mt-4'>
                        <img src='/image/success.svg' alt='Icon Success' style={{ height: '48px' }} />
                        <p  className='font-bold b-14-b mt-3 text-center'>Pembayaran Berhasil</p>
                        <p className='id text-center'>Tunjukkan invoice ini ke petugas BCR di titik temu.</p>

                        <div className="card card-invoice w-75 mx-auto mt-4">
                            <div className="d-flex justify-between">
                                <div className="card-body">
                                    <p className='font-bold'>Invoice</p>
                                    <p className='mt-3 id'>*no invoice</p>                   
                                </div>
                                <div className="align-items-end">
                                    <Download>
                                        {(props) => (
                                            <button
                                            className="download-invoice mt-3 mx-3 px-4 py-2 border-2 fw-bold rounded-1"
                                            onClick={props.onClick}
                                            >
                                            <img
                                                className="me-2 align-items-center"
                                                src='/image/fi_download.svg'
                                                alt=""
                                                width="18"
                                                height="18"
                                                />
                                            Unduh
                                            </button>
                                        )}

                                    </Download>
                                </div>
                            </div>
                            <div
                                className="d-flex w-100 border-secondary p-3 justify-content-center "
                                style={{ borderStyle: "dashed", backgroundColor: "#D0D0D0" }}
                            >
                            
                                <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.14.305/build/pdf.worker.min.js">
                                    <Viewer fileUrl={file} plugins={[getFilePluginInstance]} />
                                </Worker>
                            </div>                                                  

                        </div>
                    </div>
                </div>
            </div>

        </Fragment>

        </>
    )
}

export default Tiket