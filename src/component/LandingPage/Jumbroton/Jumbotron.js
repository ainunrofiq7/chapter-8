import React,{Fragment} from 'react';
import "./Jumbotron.css";
// import Navbar from '../Navbar/Navbar';
import About from '../About/About';
import WhyUs from '../WhyUs/WhyUs';
import PieChart from '../PieChart/PieChart';
import CtaBanner from '../ContentBlue/CtaBanner';
import Footer from '../Footer/Footer';


function Jumbotron() {
  return(
    <Fragment>
        {/* <Navbar/> */}
          <div className="bg-1" >
              <div className="d-flex justify-content-center align-items-center w-100 h-100">
                  <div className="column-hero kiri">
                      <h1 className="h-36-b">Sewa & Rental Mobil Terbaik di kawasan Banyuwangi</h1>
                      <p className="b-14-l text-hero">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                  </div>
                  <div className='car-img'>
                   <img src="/image/img_car.png" alt=""/>
                 </div>
              </div>
          </div>
          <About/>
          <WhyUs/>
          <PieChart/>
          <CtaBanner/>
          <Footer/>
        </Fragment>
  )
}


export default Jumbotron;
