import React from "react";
import './WhyUs.css'

function WhyUs (){
    return(
        <section class="us-section">
            <div class="container " >
              <div class="why-us" >
                <h3>Why Us?</h3>
                <p>Mengapa harus pilih Binar Car Rental?</p>
              </div> 
              <div class="row g-4 row-cols-1 row-cols-lg-5" >
                <div class="feature col">
                  <div class="feature-icon ">
                    <img src="/image/icon_complete1.svg" alt="icon"/>
                  </div>
                  <h2>Mobil Lengkap</h2>
                  <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                  
                </div>
                <div class="feature col">
                  <div class="feature-icon">
                    <img src="/image/icon_price.svg" alt="icon"/>
                  </div>
                  <h2>Harga Murah</h2>
                  <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                  
                </div>
                <div class="feature col">
                  <div class="feature-icon ">
                    <img src="/image/icon_24hrs.svg" alt="icon" />
                  </div>
                  <h2>Layanan 24 Jam</h2>
                    <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                    
                </div>
                <div class="feature col">
                  <div class="feature-icon ">
                    <img src="/image/icon_professional.svg" alt="icon" />
                  </div>
                  <h2>Sopir Profesional</h2>
                    <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                </div>
                
              </div>
                  
            </div>
          </section>
    )
}
export default WhyUs;