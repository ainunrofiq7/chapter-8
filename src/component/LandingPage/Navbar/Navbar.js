import React from "react";

import "./Navbar.css";

function Navbar() {
  return(
      <div className="navbar">
      <nav className="navbar-container">
          <div className="logo">
            <a href="/dashboard">
                <img class="rounded" src="/image/logo.png" alt="logo small" />
              </a>
          </div>
          <div className="navbar-menu">
              <ul className="navbar-links">
                  <li className="navbar-item"><a className="navbar-link b-14-r" href="#our-services">Our Services</a></li>
                  <li className="navbar-item"><a className="navbar-link b-14-r" href="#why-us">Why Us</a></li>
                  <li className="navbar-item"><a className="navbar-link b-14-r" href="#testimonial">Testimonial</a></li>
                  <li className="navbar-item"><a className="navbar-link b-14-r" href="#faq">FAQ</a></li>
                  <li className="navbar-item"><a className="navbar-link btn btn-success text-white b-14-b" href="/list">Register</a></li>
              </ul>
          </div>
      </nav>
  </div>

  )
}
export default Navbar
