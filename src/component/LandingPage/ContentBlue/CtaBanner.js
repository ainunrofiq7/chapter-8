import React from "react";
import './Cta.css'

function CtaBanner(){
    return(
        <section class="cta-banner">
            <h1>Sewa Mobil di Banyuwangi Sekarang</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <button class="btn btn-green" type="button">Mulai Sewa Mobil</button>
    </section>
    )
}
export default CtaBanner;