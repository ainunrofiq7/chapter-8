import React from "react";
import './About.css'

function About (){
    return(
        <section class="about-section">
            <div class="container">
              <div class="row align-items-flex-start d-flex">
                <div class="col-6">
                  <div class="happy">
                    <img src="/image/happy_.png " alt="happy"/>
                  </div>
                </div>
                <div class="col-6 " >
                  <div class="rent">
                    <h1>Best Car Rental for any kind of trip in (Lokasimu)!</h1>
                    <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                  </div>
                    <div class="list">
                      <div class="row d-flex align-items-flex-end">
                        <div class="col-6">
                          <img src="/image/Ellipse 22.png" alt="ellips"/>  
                          <p> Sewa Mobil Dengan Supir di Bali 12 Jam</p> 
                        </div>        
                      </div>
                      <div class="row d-flex align-items-flex-end">
                        <div class="col-6">
                          <img src="/image/Ellipse 22.png" alt="ellips2"/>  
                          <p> Sewa Mobil Lepas Kunci di Bali 24 Jam</p> 
                        </div>        
                      </div>
                      <div class="row d-flex align-items-flex-end">
                        <div class="col-6">
                          <img src="/image/Ellipse 22.png" alt="ellips3"/>  
                          <p>Sewa Mobil Jangka Panjang Bulanan</p> 
                        </div>        
                      </div>
                      <div class="row d-flex align-items-flex-end">
                        <div class="col-6">
                          <img src="/image/Ellipse 22.png" alt=""/>  
                          <p>Gratis Antar - Jemput Mobil di Bandara</p> 
                        </div>        
                      </div>
                      <div class="row d-flex align-items-flex-end">
                        <div class="col-6">
                          <img src="/image/Ellipse 22.png" alt=""/>  
                          <p>Layanan Airport Transfer / Drop In Out</p> 
                        </div>        
                      </div>

                    </div>        
                </div>
              </div>
            </div>
          </section>
    );
}
export default About;