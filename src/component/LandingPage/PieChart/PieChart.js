import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';

function PieChart (){
    ChartJS.register(ArcElement, Tooltip, Legend); 

    const options = {
        responsive: true,
        plugins: {
          legend: {
            position: "top",
          },
        },
      };

        const data = {
            labels: ['Avanza', 'Ertiga', 'Isuzu', 'Kijang', 'Innova'],
            datasets: [
                {
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                        "#FF6384",
                        "#63FF84",
                        "#84FF63",
                        "#8463FF",
                        "#6384FF"
                ],
                borderColor: [
                    "black"
                ],
                borderWidth: 2,
                },
            ],
        }
    return(
        <div className="chart w-25 h-25 mx-auto">
        <h2 className="text-center mb-3">Data Sewa Mobil</h2>
        <Pie options={options} data={data} />
      </div>
    );
}
export default PieChart;