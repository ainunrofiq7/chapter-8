import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import './Footer.css'

function Footer(){
    return (
    <div className='mt-5'>
            <Container>
                <Row>
                    <Col lg="3" className='fw-3'>
                        <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </Col>
                    <Col lg="2" className='fw-4'>
                        <p>Our Service</p>
                        <p>Why Us</p>
                        <p>Testimonial</p>
                        <p>FAQ</p>
                    </Col>
                    <Col lg="4" className='fw-3'>
                        <p>Connect with us</p>
                        <div className='container-icon d-flex flex-row sosmed'>
                                <img src='/image/icon_facebook.svg' alt='Icon Facebook'/>
                                <img src='/image/icon_instagram.svg' alt='Icon Instagram'/>
                                <img src='/image/icon_twitter.svg' alt='Icon Twitter'/>
                                <img src='/image/icon_mail.svg' alt='Icon Email'/>
                                <img src='/image/icon_twitch.svg' alt='Icon Twitch'/>
                        </div>
                    </Col>
                    <Col lg="3" className='fw-3'>
                        <p>Copyright Binar 2022</p>
                        <img src='/image/logo.png' alt='logo'></img>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default Footer