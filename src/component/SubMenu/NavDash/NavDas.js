import React from "react";


function NavDash (){
    return(
        <nav class="navbar sticky-top navbar-light bg-white shadow-sm px-2" style={{ zIndex: "100" }}>
        <div class="container-fluid">
          <a href="#">
            <img src="/image/fi_menu.svg" alt="" width="30" height="24"/>
          </a>
          <div class="d-flex">
            <form class="d-flex">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
              <button class="btn btn-outline-primary fw-bold" type="submit">Search</button>
            </form>
            <div class="ms-4">
              <img src="/image/user.svg" alt="user" sizes="24"/>
              <span>Unis Badri</span>
              <img src="/image/fi_chevron-down.svg" alt="dropdown"/>
            </div>
          </div>
        </div>
        </nav>
    )
}
export default NavDash;