import React from "react";
import './List.css'
import { Link } from "react-router-dom";

function List(){
    return(
      <>
      <p className="m-3">
          <span className="fw-bold">Cars</span>{" "}
          <img src="/image/chevron-right.svg" alt="right" /> List Cars
        </p>
        <div className="d-flex m-3 justify-content-between">
          <h2 className="list-car">List Car</h2>
          <Link to="/dashboard/listCar/add">
            <button className="btn-add-new-car px-4 border-0 fw-bold">
              <img src="/image/fi_plus.svg" alt="plus" className="me-3" /> Add New Car
            </button>
          </Link>
        </div>
        <div className="btn-category mx-3 d-flex gap-2 mt-3">
          <button className="active px-3 fw-bold border-1 solid rounded-1">
            All
          </button>
          <button className="non-active px-3 fw-bold border-1 solid rounded-1">
            Small
          </button>
          <button className="non-active px-3 fw-bold border-1 solid rounded-1">
            Medium
          </button>
          <button className="non-active px-3 fw-bold border-1 solid rounded-1">
            Large
          </button>
        </div>
          </>
      //   <div class="hero w-100 px-4 py-3" >
      //   <nav aria-label="breadcrumb">
      //     <ol class="breadcrumb fw-bold">
      //       <li class="breadcrumb-item"><a href="/dashboard/listCar" >Cars</a><img src="/image/fi_chevron-right.svg" alt="right"></img></li>
      //       <li class="breadcrumb-item"><a href="#" >List Car</a></li>
      //     </ol>
      //   </nav>
      //   <div class="d-flex justify-content-between align-items-center">
      //     <h3>List Car</h3>
      //     <a href="/add" class="btn btn-primary">
      //       <span class="fw-bold">+ Add New Car</span>
      //     </a>
      //   </div>
      //   <div class="d-flex  mt-3">
      //     <button class="btn bg-light btn-outline-primary btn-sm px-2 fw-bold me-2">All</button>
      //     <button class="btn bg-light btn-outline-primary btn-sm px-2 fw-bold me-2">Small</button>
      //     <button class="btn bg-light btn-outline-primary btn-sm px-2 fw-bold me-2">Medium</button>
      //     <button class="btn bg-light btn-outline-primary btn-sm px-2 fw-bold me-2">Large</button>
      //   </div>
      //   <div class="mt-4 d-flex gap-3 flex-wrap">
      //   </div>
      // </div>
    )
}
export default List;