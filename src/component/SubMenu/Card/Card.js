import React from "react";
import { BsPeople } from "react-icons/bs";
import { FiSettings } from "react-icons/fi";
import { FaRegCalendar } from "react-icons/fa";

import "./Card.css";

const Card = (props) => {
  return (
    <div className="">
      <div key="{e.no}" className="card my-3 ">
        <div className="wrap-img-card p-4">
          <div className="layer-img-card d-flex">
            <img src={props.image} className="card-img-top my-auto" alt="..." />
          </div>
        </div>
        <div className="card-body">
        <div className="detail d-flex flex-column justify-content-between">
                  <p className="p1">{props.name}</p>
                  <p className="p2">Rp {props.price} / hari</p>
                  <p className="p3">
                    <img src="/image/fi_key.svg" /> Start rent - Finish rent
                  </p>
                  <p className="p3">
                    <img src="/image/fi_clock.svg" /> Updated at 4 Apr 2022, 09.00
                  </p>
                </div>
          <div className="d-flex justify-content-between mt-4">
            <form class="d-flex flex-grow-1" action="#" method="post">
              <button type="button" class="d-flex flex-grow-1 justify-content-center btn btn-lg btn-outline-danger w-50 me-2" data-bs-toggle="modal" data-bs-target="#exampleModal<%= car.id %>">
                <img className="mt-1 mx-2" src="/image/fi_trash-2.svg" alt="trash" srcset=""/>
                <span class="fw-bold ms-1" >Delete</span>
              </button>
            </form>
            <form class="d-flex flex-grow-1" action="/edit/<%= car.id %>" method="get">
              <button type="submit" class="d-flex flex-grow-1 justify-content-center btn btn-lg btn-success w-50 ms-2">
                <img className="mt-1 mx-2" src="/image/fi_edit.svg" alt="edit" srcset=""/>
                <span class="fw-bold ms-1">Edit</span>
              </button>
           </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
