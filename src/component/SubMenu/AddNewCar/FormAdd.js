import React, { useState } from "react";
import { useNavigate } from "react-router";
import './Form.css'
import { Helmet } from "react-helmet";
import { Col, Row, Form } from "react-bootstrap";

import { Uploader } from "uploader";
import { UploadDropzone } from "react-uploader";
import { Link } from "react-router-dom";
import axios from "axios";
import { message } from "antd";

function NewCar(){

    const title = "Add New Car - Binar Car Rental";
    const navigate = useNavigate();

    const success = () => {
        message.success("Data Berhasil Ditambahkan!");
      };

  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [img, setImg] = useState(null);
  console.log(img);

  const handleCreateNewCar = async (e) => {
    e.preventDefault();
    const data = new FormData();
    data.append("name", name);
    data.append("category", "Small");
    data.append("price", price);
    data.append("image", img[0].originalFile.file);

    const config = {
        headers: {
          "content-type": "multipart/form-data",
        },
      };
    try {
      await axios.post(`https://rent-cars-api.herokuapp.com/admin/car`, data, config).then((response) => {
          console.log(response);
          success();
          navigate("/dashboard/listCar");
      });
    }catch (error) {
      console.log(error);
      alert("Can't Post the data!");
    }
    }
  
  const uploader = new Uploader({
    //free api key getted after sign in
    apiKey: "public_12a1xjx6DVMv6D9qbjunZZdSEfwJ",
  });


    return(   
        <div className="content-add-new-car  ">
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <div>
        <p className="mb-4">
          <Link
            to="/dashboard/listCar"
            className="text-decoration-none text-black"
          >
            <span className="fw-bold">Cars</span>
            <img src="/image/chevron-right.svg" alt="right" />
          </Link>
          <Link
            to="/dashboard/listCar"
            className="text-decoration-none text-black"
          >
            <span className="fw-bold">List Car</span>
            <img src="/image/chevron-right.svg" alt="right" />
          </Link>
          Add New Cars
        </p>
        <h2 className="add-car mb-3">Add New Car</h2>
      </div>
      <Form onSubmit={handleCreateNewCar}>
        <div className="card-form  bg-white shadow-sm px-4 pt-4 pb-1 mb-0">
          <div className="form">
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2" className="label-form">
                Nama<span className="text-danger">*</span>
              </Form.Label>
              <Col sm="5" className="px-4">
                <Form.Control
                  id="name"
                  className="input-form"
                  type="text"
                  placeholder="Car Name"
                  
                />
              </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2" className="label-form">
                Category<span className="text-danger">*</span>
              </Form.Label>
              <Col sm="5" className="px-4">
                <Form.Control
                  id="category"
                  className="input-form"
                  type="text"
                  placeholder="Car Category Capacity"
                  
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2" className="label-form">
                Harga<span className="text-danger">*</span>
              </Form.Label>
              <Col sm="5" className="px-4">
                <Form.Control
                  id="price"
                  className="input-form"
                  type="text"
                  placeholder="Car Price"
                 
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2" className="label-form">
                Rent Status<span className="text-danger">*</span>
              </Form.Label>
              <Col sm="5" className="px-4 ">
                <Form.Select id="status" >
                  <option className="default-text">
                    The car is renting or not ?
                  </option>
                  <option value={true} className="value-status">
                    Yes
                  </option>
                  <option value={false} className="value-status">
                    No
                  </option>
                </Form.Select>
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3 ">
              <Form.Label column sm="2" className="label-form">
                Foto<span className="text-danger">*</span>
              </Form.Label>
              <Col sm="5">

                <UploadDropzone
                  uploader={uploader}
                  options={{ multi: false }}
                  onUpdate={(files) => setImg(files)}
                  width="600px"
                  height="220px"
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2" className="label-form">
                Start Rent
              </Form.Label>
              <Col sm="5" className="px-4">
                <p>-</p>
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3t">
              <Form.Label column sm="2" className="label-form">
                Finish Rent
              </Form.Label>
              <Col sm="5" className="px-4">
                <p>-</p>
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2" className="label-form">
                Created At
              </Form.Label>
              <Col sm="5" className="px-4">
                <p>-</p>
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2" className="label-form">
                Updated At
              </Form.Label>
              <Col sm="5" className="px-4">
                <p>-</p>
              </Col>
            </Form.Group>
          </div>
        </div>
        <div className="btn mt-5">
          <button type="reset" className="cancel px-4 me-3">
            Cancel
          </button>
          <button type="submit" className="save px-4 border-0 text-white">
            Save
          </button>
        </div>
      </Form>
    </div>
    )

}
export default NewCar;