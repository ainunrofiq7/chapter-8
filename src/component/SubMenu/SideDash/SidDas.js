import React from "react";
import './SideDash.css'
import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom";

function SideDash (){
  const location = useLocation();
  console.log(location.pathname);
  return (
    <section>
      <section className="sidebar">
        <div className="sidebarIcon position-absolute">
          <div className="top d-flex align-items-center justify-content-center">
            <div className="img"></div>
          </div>
          <Link to="/dashboard " className="text-decoration-none">
            <div className={"menu overflow-hidden  align-items-center justify-content-center flex-column" + 
            (location.pathname === "dashboard" || 
            location.pathname === "/dashboard/" ? "active-state" : "nonActive-state")}  >
              <img className="mx-3" src="/image/fi_home.svg"/>
              <p>Dashboard</p>
            </div>
          </Link>
          <Link to="/dashboard/listCar " className="text-decoration-none" >
            <div className={"menu overflow-hidden align-items-center justify-content-center flex-column" + 
                        (location.pathname === "/dashboard/listCar" ||
                        location.pathname === "/dashboard/listCar/addNewCar"
                      ? "active-state"
                      : "nonActive-state") }>
              <img className="mx-3" src="/image/fi_truck.svg"/>
              <p>Cars</p>
            </div>
          </Link>
        </div>

        <div className="sidebarMenu">
          <div className="top d-flex align-items-center">
            <div className="logo"></div>
          </div>
            <div className="menu">
              {location.pathname === "/dashboard" ?(
              <>
                <div class="title py-2 ms-4 mt-2 d-flex flex-column" >
                  <span class="text-secondary fs-5 fw-bold">Dashboard</span>
                </div>
              <div class="menu py-2 mt-1">
                
                  <div class="d-flex flex-column justify-content-center px-4">
                    <span class="text-dark fw-bold">Dashboard</span>
                  </div>
                
              </div>
              </>
              ) : (
                <>
                <div class="title py-2 ms-4 mt-2 d-flex flex-column" >
                <span class="text-secondary fs-5 fw-bold">Cars</span>
              </div>
              <div class="menu py-2 mt-1">
                
                  <div class="d-flex flex-column justify-content-center px-4">
                    <span class="text-dark fw-bold">List Car</span>
                  </div>
                
              </div>
              </>
            )}
            </div>
          </div>

      </section>
    </section>
  );
  
}
export default SideDash;