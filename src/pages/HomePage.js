import React, { Fragment } from "react";
import { Helmet } from "react-helmet";
import Navbar from "../component/LandingPage/Navbar/Navbar";

import Jumbotron from "../component/LandingPage/Jumbroton/Jumbotron";

function LandingPages() {
    const title = "Home - Binar Car Rental";
    return(
        <>
        <Helmet>
            <title>{title}</title>
        </Helmet>
            <Navbar/>    
            <Jumbotron/>
        </>

    )
}
export default LandingPages;