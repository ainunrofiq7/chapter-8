import React from "react";
import NavDash from "../component/SubMenu/NavDash/NavDas";
import NewCar from "../component/SubMenu/AddNewCar/FormAdd";
import SideDash from "../component/SubMenu/SideDash/SidDas";

function AddPage () {
    return(
        <div className="dashboard d-flex ">
            <SideDash />
            <div className="w-100 justify-content-start position-relative">
            <NavDash/>           
            <NewCar/>

            </div>

        </div>
    

    )
}
export default AddPage;