import React, { Fragment } from "react";
import Tiket from "../component/Tiket/Tiket";
import Navbar from "../component/LandingPage/Navbar/Navbar";
import Footer from "../component/LandingPage/Footer/Footer";

function InvoicePage() {
    return(
        <Fragment>
            <Navbar/>
            <Tiket/>
            <Footer/>
        </Fragment>

    )
}
export default InvoicePage;