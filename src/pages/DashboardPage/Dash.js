import React from "react";
import NavDash from "../../component/SubMenu/NavDash/NavDas";
import SideDash from "../../component/SubMenu/SideDash/SidDas";
import { Outlet } from "react-router";

const PageDashboard = () =>{
    return(
        <div className="dashboard d-flex w-100">
            <SideDash />
            <div className=" justify-content-start w-100 gap-0 position-relative">
            <NavDash/>
                <Outlet />
            </div>

        </div>
    )
}
export default PageDashboard;
