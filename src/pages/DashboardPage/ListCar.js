import React from "react";
import List from "../../component/SubMenu/List/List";
import Card from "../../component/SubMenu/Card/Card";
import { Container, Row, Col } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getCarList } from "../../stores/actions/carAction";

function ListPage() {
  const { getListCarResult } = useSelector((state) => state.carReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCarList());
  }, [dispatch]);
  console.log(getListCarResult);

  return (

     <div class="d-flex bg-secondary ">
      <div class="flex-grow-1 " style={{ height: "100vh" }}>
        <List/>
            <main className="mt-4">
            <Container>
              <Row>
                {getListCarResult.length > 0 &&
                  getListCarResult.map((e, key) => {
                    return (  
                      <Col md="4">
                        <Card key={key} {...e} />
                      </Col>
                    );
                  })}
              </Row>
            </Container>
          </main>
        </div>
      </div>
  );
}

export default ListPage;
